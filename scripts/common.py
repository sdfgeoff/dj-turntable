import bge

def get_full_path(path):
    '''Returns the path relative to the blend'''
    return bge.logic.expandPath(path)
