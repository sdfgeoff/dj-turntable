'''This module handles complex audio interactions such as playing tracks
at varying speeds and seeking to specified positions'''

import wave
import contextlib

import aud

import common

DEVICE = aud.device()

class Track(object):
    '''Anm object to manage a audio track that needs to be manipulated'''
    def __init__(self, path):
        self.path = path
        self.speed = 0.0
        self.fwd_factory = aud.Factory.file(self.path).buffer()
        self.rev_factory = self.fwd_factory.reverse().buffer()
        self.length = self._compute_track_length()

        self.forwards = DEVICE.play(self.fwd_factory)
        self.forwards.keep = True

        self.backwards = DEVICE.play(self.rev_factory)
        self.backwards.keep = True

    def seek(self, pos):
        '''Sets playback location to the specified number of seconds'''
        if pos > self.length-0.5: #A hack to stop the track becoming invalid if time goes negative!
            pos = 0.0
        self.forwards.position = max(pos, 0.0)
        self.backwards.position = max(self.length - pos, 0.0)

    def set_speed(self, speed):
        '''Sets the speed of playback as a percentage of normal'''
        current_pos = self.get_position()
        if abs(speed) < 0.0001:
            self.pause()
        elif speed > 0:
            # Playing forwards
            if self.speed <= 0:
                #If direction has changed, do the right thing
                self.backwards.pause()
                self.forwards.resume()
                self.seek(current_pos)

            self.forwards.pitch = speed
            self.backwards.pitch = speed
        else:
            # Playing backwards
            if self.speed >= 0:
                #If direction has changed, do the right thing
                self.forwards.pause()
                self.backwards.resume()
                self.seek(current_pos)

            self.forwards.pitch = abs(speed)
            self.backwards.pitch = abs(speed)

        self.speed = speed

    def pause(self):
        '''Pauses the track. To resume, set the speed'''
        self.backwards.pause()
        self.forwards.pause()
        self.speed = 0

    def set_volume(self, vol):
        '''Updates the volume of the track'''
        self.forwards.volume = vol
        self.backwards.volume = vol

    def get_position(self):
        '''Returns the current position of the track in seconds from track start'''
        if self.speed >= 0:
            return self.forwards.position
        else:
            return self.length - self.backwards.position

    def _compute_track_length(self):
        '''Returns a float representing the length of the track in
        seconds. Don't call this function, use the .length property

        Note that this disagrees with aud sometimes'''
        try:
            with contextlib.closing(wave.open(self.path, 'r')) as media:
                frames = media.getnframes()
                rate = media.getframerate()
                track_duration = frames / float(rate)
                return track_duration
        except wave.Error:
            print("Error finding file duration, probably not a .wav file")
            return 0.0


def test(_):
    '''Runs a simple test on the audio engine by playing a track backwards'''
    tester = Track(common.get_full_path('//SampleMusic/Sleeping Child.wav'))
    tester.seek(10.0)
    tester.set_speed(-0.5)
