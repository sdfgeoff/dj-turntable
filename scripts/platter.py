'''This module handles a platter in the simulation.'''
import audio
import common


MOTOR_GAIN = 0.5       #Governs smoothness of platter approaching normal speed
MOTOR_MAX_TORQUE = 1.0 #Governs smoothness of platter when motor is engaged

def init_platter(cont):
    '''Loads the audio file!'''
    file_name = cont.owner['filepath']
    full_path = common.get_full_path(file_name)
    cont.owner['track'] = audio.Track(full_path)
    cont.owner['track'].set_speed(0.0)

    for child in cont.owner.children[0].groupMembers:
        child.setParent(cont.owner)
    print(cont.owner.scene.objects)



def update_pitch(cont):
    '''updates the audio pitch based on the rotation speed'''
    rot_speed = cont.owner.worldAngularVelocity.z
    cont.owner['track'].set_speed(rot_speed)


def to_normal_speed(cont):
    '''Transitions the platter to the normal track speed'''
    existing_damping = cont.owner.angularDamping
    current_speed = cont.owner.worldAngularVelocity.z

    normal_speed = (1.0 / (1.0 - existing_damping) + 1)/2

    out_torque = (normal_speed - current_speed) * MOTOR_GAIN
    out_torque = max(-MOTOR_MAX_TORQUE, min(MOTOR_MAX_TORQUE, out_torque))
    cont.owner.applyTorque([0, 0, out_torque], False)
